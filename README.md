# Vending machine

Developed on: `MacOS Monterey 12.6; Node v14.19.0;`

Run and compile:

```sh
yarn
yarn dev
```

You can access the app in `localhost:8080`

Port can be changed in `vite.config.ts`

Accepted denominations: `1,2,5,10,20`

Machine keeps track of available coins in the bank.
You can check them in Vue devtools inspecting the store `main.coinBank` state.

There is no validation on the user input.

There is support for `dark` and `light` mode in the browser.
