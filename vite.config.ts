import { fileURLToPath, URL } from "node:url";
import eslint from "vite-plugin-eslint";

import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  css: {
    preprocessorOptions: { scss: { charset: false }, css: { charset: false } }
  },
  server: {
    port: 8080
  },
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url))
    }
  }
});
