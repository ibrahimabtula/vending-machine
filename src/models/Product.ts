import type IEntity from "./Entity";

export default interface IProduct extends IEntity {
  name: string;
  quantity: number;
  price: number;
  image: string;
}
