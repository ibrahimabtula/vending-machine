import type ICoinBankBankItem from "@/models/CoinBankItem";

export default interface ICoinBank {
  [key: string]: ICoinBankBankItem;
}
