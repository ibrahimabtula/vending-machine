import type IEntity from "./Entity";

export default interface IApi<T extends IEntity> {
  list: () => Promise<T[]>;
  create: (data: Partial<T>) => Promise<T>;
  getById: (id: T["id"]) => Promise<T | undefined>;
  update: (id: T["id"], data: Partial<T>) => Promise<T>;
  deleteById: (id: T["id"]) => Promise<T["id"]>;
}
