export default interface ICoinBankBankItem {
  denomination: number;
  quantity: number;
}
