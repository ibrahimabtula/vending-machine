import mockProducts from "@/mockData/products.json";
import type IApi from "@/models/Api";
import type IProduct from "@/models/Product";

export default function useProduct(): IApi<IProduct> {
  // GET
  const list = () => Promise.resolve(mockProducts as IProduct[]);

  // POST
  const create = async (data: Partial<IProduct>) => {
    const lastId = mockProducts.map((i) => i.id).sort()?.[
      mockProducts.length - 1
    ];
    mockProducts.push({ ...data, id: lastId + 1 } as IProduct);
    return Promise.resolve(mockProducts[mockProducts.length - 1] as IProduct);
  };

  // GET ID
  const getById = (id: IProduct["id"]) =>
    Promise.resolve(
      mockProducts.find((i) => i.id === id) as IProduct | undefined
    );

  // PUT
  const update = async (id: IProduct["id"], data: Partial<IProduct>) => {
    const product = await getById(id);
    const index = mockProducts.findIndex((i) => i.id === id);
    mockProducts[index] = { ...product, ...(data as IProduct) };
    return mockProducts[index] as IProduct;
  };

  // DELETE
  const deleteById = (id: IProduct["id"]) => {
    const index = mockProducts.findIndex((i) => i.id === id);
    mockProducts.splice(index, 1);
    return Promise.resolve(id);
  };

  return {
    list,
    getById,
    create,
    update,
    deleteById
  };
}
