import { createRouter, createWebHistory } from "vue-router";
import MachineView from "@/views/MachineView.vue";
import ProductsView from "@/views/ProductsView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "machine",
      component: MachineView
    },
    {
      path: "/products",
      name: "products",
      component: ProductsView
    }
  ]
});

export default router;
