import { defineStore } from "pinia";
import useProductApi from "@/api/useProducts";
import type IProduct from "@/models/Product";
import type ICoinBank from "@/models/CoinBank";
import type ICoinBankBankItem from "@/models/CoinBankItem";

const { list, getById, create, update, deleteById } = useProductApi();

export const useStore = defineStore("main", {
  state: () => ({
    /** The coinBank represents all available denomination (coins) in the machine */
    coinBank: {
      "1": { denomination: 1, quantity: 5 },
      "2": { denomination: 2, quantity: 50 },
      "5": { denomination: 5, quantity: 10 },
      "10": { denomination: 10, quantity: 10 },
      "20": { denomination: 20, quantity: 10 }
    } as ICoinBank,
    insertedCoins: [] as number[],
    products: [] as IProduct[],
    change: [] as number[],
    crtScreenMessage: "",
    formatter: new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD"
    })
  }),
  getters: {
    denominations: (state) =>
      Object.keys(state.coinBank).map((i) => state.coinBank[i].denomination),
    insertedTotal: (state) =>
      state.insertedCoins.reduce((sum, i) => (sum += i), 0)
  },
  actions: {
    async fetchProducts() {
      const products = await list();
      this.products.splice(0, this.products.length);
      products.forEach((i) => this.products.push(i));
      return this.products;
    },
    getById(id: IProduct["id"]) {
      return getById(id);
    },
    create({ data }: { data: Partial<IProduct> }) {
      return create(data);
    },
    update({ id, data }: { id: IProduct["id"]; data: Partial<IProduct> }) {
      return update(id, data);
    },
    deleteById(id: IProduct["id"]) {
      return deleteById(id);
    },
    refreshList() {
      return this.fetchProducts();
    },
    async orderProduct(product: IProduct) {
      try {
        const dbProduct = await getById(product.id);

        if (this.insertedTotal === 0) throw new Error("Please insert coins");
        if (!dbProduct) throw new Error("Product not found");
        if (dbProduct.quantity === 0) throw new Error("Product not available");

        const chargeAmount = product.price;
        const change = this.insertedTotal - chargeAmount;

        if (this.insertedTotal < product.price)
          throw new Error("Please insert more coins");

        /** Clear previous change if any before start */
        this.change.splice(0, this.change.length);

        /** Calculate how much we must return and add to change */
        const changeToReturn = this.tryCalculateChange(change);

        if (!changeToReturn.length && change) {
          this.returnCoins();
          throw new Error("Not enough coins");
        }

        changeToReturn.forEach((i) => {
          this.change.push(i);
          this.coinBank[`${i}`].quantity--;
        });

        /** Clear and add inserted coins to the bank */
        this.insertedCoins.forEach((coin) => {
          this.coinBank[coin.toString()].quantity++;
        });
        this.insertedCoins.splice(0, this.insertedCoins.length);

        /** Wait 2s for better UX */
        await new Promise((resolve) => {
          this.crtScreenMessage = `Preparing!`;
          setTimeout(resolve, 1500);
        });
        this.crtScreenMessage = `${product.name} dispatched!`;

        return update(dbProduct.id, {
          ...dbProduct,
          quantity: dbProduct.quantity - 1
        });
      } catch (error) {
        this.crtScreenMessage = (error as Error).message;
      }
    },
    insertCoin(coin: number) {
      try {
        if (!this.coinBank[`${coin}`]) throw new Error("Unknown denomination!");

        this.insertedCoins.push(coin);
        this.crtScreenMessage = this.formatter.format(this.insertedTotal);
      } catch (error) {
        this.crtScreenMessage = (error as Error).message;
      }
    },
    tryCalculateChange(change: number) {
      const getAvailableCoins = (coinBank: ICoinBank) =>
        Object.keys(coinBank)
          .filter((i: string) => coinBank[i].quantity > 0)
          .map((i) => coinBank[i]);

      const minNumberOfCoins = (items: ICoinBankBankItem[], change: number) => {
        const res: number[] = [];

        if (change === 0) return res;

        for (let i = items.length - 1; i >= 0; i--) {
          while (change >= items[i].denomination) {
            change -= items[i].denomination;
            res.push(items[i].denomination);
          }
        }

        return res;
      };

      return minNumberOfCoins(getAvailableCoins(this.coinBank), change);

    },
    returnCoins() {
      try {
        /** Clear change */
        this.change.splice(0, this.change.length);

        if (!this.insertedCoins.length) {
          throw new Error("No coins to return!");
        }
        /** Return inserted coins as change */
        this.insertedCoins.forEach((coin) => {
          this.change.push(coin);
        });
        this.insertedCoins.splice(0, this.insertedCoins.length);
        this.crtScreenMessage = "Please take your change!";
      } catch (error) {
        this.crtScreenMessage = (error as Error).message;
      }
    }
  }
});
